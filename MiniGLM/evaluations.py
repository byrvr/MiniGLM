import os
import json
import torch
from model import GLMConfig, MiniGLM
import tiktoken
from rouge import Rouge

class nullcontext:
    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

# -----------------------------------------------------------------------------

out_dir = 'out-1694866363'
start = ""
num_samples = 10
max_new_tokens = 500
temperature = 0.8
top_k = 200
seed = 1234
device = 'cpu'
dtype = 'bfloat16' if torch.cuda.is_available() and torch.cuda.is_bf16_supported() else 'float16'
compile = False

torch.manual_seed(seed)
torch.cuda.manual_seed(seed)
torch.backends.cuda.matmul.allow_tf32 = True
torch.backends.cudnn.allow_tf32 = True
device_type = 'cuda' if 'cuda' in device else 'cpu'
ptdtype = {'float32': torch.float32, 'bfloat16': torch.bfloat16, 'float16': torch.float16}[dtype]
ctx = nullcontext() if device_type == 'cpu' else torch.amp.autocast(device_type=device_type, dtype=ptdtype)

ckpt_path = os.path.join(out_dir, 'ckpt.pt')
checkpoint = torch.load(ckpt_path, map_location=device)
config = GLMConfig(**checkpoint['model_args'])
model = MiniGLM(config)
state_dict = checkpoint['model']
unwanted_prefix = '_orig_mod.'
for k, v in list(state_dict.items()):
    if k.startswith(unwanted_prefix):
        state_dict[k[len(unwanted_prefix):]] = state_dict.pop(k)
model.load_state_dict(state_dict, strict=False)

model.eval()
model.to(device)

enc = tiktoken.get_encoding("gpt2")
encode = lambda s: enc.encode(s, allowed_special={""})
decode = lambda l: enc.decode(l)

# Read questions from eval.jsonl to generate answers
print("Reading questions...")
questions = []
with open('data/eval.jsonl', 'r') as f:
    for line in f:
        data = json.loads(line)
        questions.append(data['Question'].strip())

# Generate answers for each question
generated_answers = []
for idx, question in enumerate(questions):
    start_ids = encode(question)
    x = (torch.tensor(start_ids, dtype=torch.long, device=device)[None, ...])

    with torch.no_grad():
        with ctx:
            y = model.generate(x, max_new_tokens, temperature=temperature, top_k=top_k)
            output_tokens = y[0].tolist()
            try:
                end_idx = output_tokens.index(50256)
                output_tokens = output_tokens[:end_idx]
            except:
                pass
            output = decode(output_tokens)
            generated_answers.append(output)
    print(f"Generated answer for question {idx + 1}/{len(questions)}")

# Read reference answers
print("Reading reference answers...")
references = []
with open('data/eval.jsonl', 'r') as f:
    for line in f:
        data = json.loads(line)
        references.append(data['Answer'].strip())

# Compute Rouge-L
print("Computing Rouge-L scores...")
rouge = Rouge(metrics=['rouge-l'])
scores = rouge.get_scores(generated_answers, references, avg=True)

print(scores)