# FAQ - Programming Training Second Major Assignment
## Acceptance and Submission
### Q1: What specific tasks need to be performed during on-site acceptance? Is there any additional preparation required?
A1: "On-site acceptance will take place on September 17 (Sunday) from 17:00 to 21:00 (LLM). Data analysis reports, experiment reports, and all source code must be submitted on the course assignment page of the online learning platform before 23:59 on September 17 (Sunday) evening." During acceptance, you need to demonstrate all corresponding functionalities of the "Model Dialogue".

The basic acceptance process includes:
1. Display the pre-trained model generation results.
2. Display the fine-tuned model's question-answering performance.
3. Display the loss curves for pre-training and fine-tuning.
4. Provide an introduction to the prepared fine-tuning data and collection process.
5. Prepare relevant information for generating test cases after fine-tuning (directory path, scripts, etc., see details below).

For the generation of test cases in the fifth step, students need to provide a code file named `sample.py` and a running path `/run/dir/`, and perform a unified evaluation in the following form:

```bash
cd /run/dir/
CUDA_VISIBLE_DEVICES=i python sample.py <other_params> \
--input_data=<input_path> --output_data=<output_path>
```

We provide an input path `<input_path>` as a jsonl file in the following format:

```json
{"question": "Introduce the main characters of 'The Return of the Condor Heroes'."}
{"question": "What story does 'The Legend of the Condor Heroes' tell?"}
```

The generated script should output a jsonl file at `<output_path>` in the following format:

```json
{"question": "Introduce the main characters of 'The Return of the Condor Heroes'.", "answer": "'The Return of the Condor Heroes' is a martial arts novel written by Mr. Jin Yong, with main characters including Xiao Feng, Duan Yu, and Xu Zhu."}
{"question": "What story does 'The Legend of the Condor Heroes' tell?", "answer": "'The Legend of the Condor Heroes' is a martial arts novel written by Mr. Jin Yong, depicting the growth of the two main characters, Guo Jing and Yang Kang, during the Southern Song Dynasty period."}
```

### Q2: UPDATES / BUGFIX
A2.1: The assistant has written some minor bugs on the parameter override (TAT, although it does not affect the program's operation at all). However, to use it more concisely, you can make the following modifications to the framework code:
1. The directory name for training records is now `out-shediao-{time}`. Change line 4 in `config/train_config.py` from `out_dir = f'out-{dataset}-' + str(int(time.time()))` to `out_dir = f'out-' + str(int(time.time()))` to get a cleaner output directory. (Of course, you can also customize your desired output directory name. However, be aware that if the output directories of multiple trainings are the same, they will overwrite each other. You can use `time.time()` to add a timestamp for differentiation).
2. Error when `save_path` is not specified in `sample.py`: Move line 10 in `sample.py`, `save_path = os.path.join(out_dir, 'samples')`, to be called after line 22 (i.e., after reloading the parameters with `configurator.py` and before assigning a value to `save_path`).

A2.2: Why is my fine-tuned training not saving checkpoints? This is because the parameter `always_save_checkpoint` defined on line 10 in `config/train_config.py` is set to not save the checkpoint if the current valid loss is less than the previous one. You can set it to `True` during the fine-tuning stage of training.

A2.3: The output of the model after fine-tuning doesn't stop until it reaches the maximum length limit. The provided code can perform normal training and inference. Some students have found that after fine-tuning, the model continues to output indefinitely even after answering the question. To ensure that only valid replies are retained and to let the model "know" when to stop, a special end-of-text (EOT) token needs to be appended after the answer in the fine-tuning data as a special termination indicator. The way to append the EOT token is as follows:
```python
answer = "It's a beautiful day."
enc = tiktoken.get("gpt2")
ans_tokens = enc.encode_ordinary(answer)
ans_tokens = ans_tokens + [enc.eot_token]
```
`eot_token` is a special symbol representing the end (default index is 50256, corresponding to the token ""). In the sampling process in `sample.py`, it should be checked whether `enc.eot_token` is generated (if `eot_token` is generated, the generation process should be terminated). That is, change line 70 in `sample.py` from:
```python
output = decode(y[0].tolist())
```
to:
```python
output_tokens = y[0].tolist()
try:
    end_idx = output_tokens.index(50256)
    output_tokens = output_tokens[:end_idx]
except:
    pass
output = decode(output_tokens)
```
And change line 83 in `sample.py` (before modification) from:
```python
output_tokens = y[0].tolist()
```
to:
```python
output_tokens = y[0].tolist()
try:
    end_idx = output_tokens.index(50256)
    output_tokens = output_tokens[:end_idx]
except:
    pass
output = decode(output_tokens)
```
We also provide an already modified `sample.py` that can be downloaded from [here](link_to_the_modified_file).

A2.4: Regarding the classification of test questions and examples (see assignment instructions document):
- All questions only involve "shendiao," "shediao," "tiaolong."
- Test question categories and examples:
  - Character Introduction
    - Introduce the main characters of 'The Return of the Condor Heroes'.
  - Plot
    - Tell me about the famous martial arts competition in 'The Legend of the Condor Heroes'.
  - Character Relationships
    - What is the relationship and story between Xiao Longnv and Yang Guo?
  - Others
    - What story does 'The Legend of the Condor Heroes' tell?
- Overall, the test cases used include both interrogative and imperative sentences. Interrogative sentences end with a Chinese question mark "?" and imperative sentences end with a Chinese period "。".

A2.5: Acceptance of gradio
- During the offline acceptance of gradio, students can deploy the system on a server and access the acceptance through local port forwarding via SSH, allowing access in a local browser.
- Usage of localforward, using a gradio started on the server with 127.0.0.1:7860 as an example:
```bash
ssh -L 7860:127.0.0.1:7860 xxx
```
This allows access at 127.0.0.1:7860 locally. It is also equivalent to setting the Localforward

 item in the ssh config.

### Q3: Why do I get different outputs when I ask the model (ChatGLM / ChatGPT) the same question twice?
A3: The model's generated answers inherently have a certain randomness, as the generation process involves sampling. Therefore, it can produce different outputs within a reasonable semantic range.

### Q4: Sometimes the model generates replies with errors, such as plot confusion. If I haven't personally read any works by Jin Yong, can't write data by hand, and can't quickly distinguish between correct and incorrect generated data, what should I do?
A4: Currently, when using chatglm / chatgpt, answers generated may indeed have errors or confusion. This is an inherent issue with the model. While correctness in replies is important, it is more crucial that the response is relevant to the question. If you want to improve the correctness of model responses, you can try methods such as using prompts to correct the model or averaging multiple responses. However, it may not be 100% effective. This is a relatively open-ended issue at the moment. Additionally, you can extract some questions and answers from datasets available online, such as [this dataset](https://huggingface.co/datasets/YeungNLP/firefly-train-1.1M), which contains some relevant content.

### Q5: During fine-tuning, if I write it like the pre-training data, I need to use torch.stack, but what if the lengths of different "question-answer" pairs are not the same?
A5: This requires the use of "padding" techniques, where you can use any word (token) to pad different QA pairs to the same length. However, during loss calculation, the padded parts should not be involved in the loss calculation. You will need to modify the `loss_mask` to achieve this.

### Q6: Is it necessary to use all the novel data for pre-training?
A6: It's not necessary to use all of it; you can use only a portion of the data. The final acceptance test will only cover content from the three novels mentioned in the examples: "tianlong," "shendiao," and "shediao." The range of accepted questions will be announced later.

(End of FAQ)