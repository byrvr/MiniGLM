import os
import sys
import tiktoken
import numpy as np

enc = tiktoken.get_encoding("gpt2")

names = sys.argv[1:]

# Initialize lists for training and validation data
train_data = []
val_data = []

for name in names:
    print(name)
    with open(f'{name}/input.txt', 'r', encoding='utf8') as file:
        data = file.read()
        
        # calculate the index at which to split the data
        split_idx = int(len(data) * 0.9)
        
        # split the data into training and validation sets
        train_data.append(data[:split_idx])
        val_data.append(data[split_idx:])

# Combine data from all datasets
train_data = ' '.join(train_data)
val_data = ' '.join(val_data)

# tokenize raw data with tiktoken encoder
# transform to numpy array
train_ids = np.array(enc.encode_ordinary(train_data), dtype=np.uint16)
val_ids = np.array(enc.encode_ordinary(val_data), dtype=np.uint16)

# save numpy array to file [name]/train.bin and [name]/val.bin
train_ids.tofile(os.path.join("train.bin"))
val_ids.tofile(os.path.join('val.bin'))