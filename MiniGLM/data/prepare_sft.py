import os
import sys
import json
import tiktoken
import numpy as np

enc = tiktoken.get_encoding("gpt2")

# Assuming the first argument is the path to the eval.jsonl file
input_file = ("sft_data.jsonl")
output_dir = "processed_data_sft"

# Read data from eval.jsonl
data = []
with open(input_file, "r", encoding="utf-8") as file:
    for line in file:
        entry = json.loads(line)
        formatted_entry = "问：" + entry["Question"] + "\n答：" + entry["Answer"] + "\n"
        data.append(formatted_entry)

# Combine all entries into a single string
data = ''.join(data)

# Split data for train(0.9) and valid (0.1)
split_index = int(0.9 * len(data))
train_data = data[:split_index]
val_data = data[split_index:]

# Tokenize raw data with tiktoken encoder
# Transform to numpy array
train_ids = np.array(enc.encode(train_data))
val_ids = np.array(enc.encode(val_data))

# Save numpy array to file [output_dir]/train.bin and [output_dir]/val.bin
os.makedirs(output_dir, exist_ok=True)
train_ids.tofile(os.path.join(output_dir, "train.bin"))
val_ids.tofile(os.path.join(output_dir, "val.bin"))
