# MiniGLM

The following is a list of the currently provided or outlined framework content for MiniGLM.

## Data Preprocessing

First, navigate to the data directory:
```bash
cd data/
```

- Prepare Data：
  We have provided accessible data on Network Learning, but you can also download the data using the provided script.    
    ```bash
    python fetch_data.py [dataset_names] # download
    ```
    Here, you can specify the dataset to download using the [dataset_name] parameter. Available datasets include:
    - shediao: The Legend of the Condor Heroes
    - shendiao: The Return of the Condor Heroes
    - tianlong: Demi-Gods and Semi-Devils
    
    You can also add more datasets on your own.
    
- Data Preprocessing (To be implemented):

    ```
    python prepare.py [dataset_names] # tokenize
    ```
    Specify a number of datasets using [dataset_names] and process them into a single dataset (including training set train.bin and validation set val.bin).

## Model Training

Initiate training by running the following command:
```bash
python train.py config/train_config.py --dataset=[dataset_name]
```
Here, the `--dataset` parameter specifies the subdirectory name under data/ where the data is located.

During training, the model will be automatically saved using `torch.save(checkpoint, os.path.join(out_dir, 'ckpt.pt'))`.


Fine-tuning (continuing training on the existing model) can be done using the following command:
```bash
python train.py config/train_config.py --dataset=[dataset_name] --init_from=finetune --ckpt_dir=[/path/to/ckpt/dir]
```
Here, the `--dataset` parameter specifies the subdirectory name under `data/`, and `--ckpt_dir` parameter specifies the directory location of the loaded training model.

## Model Inference

Load the trained model weights for inference using the following command:

```bash
python sample.py --out_dir=[/dir/to/training/output] --save_path=/path/to/save/output # or add prompts by --start=FILE:/path/to/prompts.txt
```

Where：
- `--out_dir`parameter specifies the directory of the model weights used (generated during the training process).

- `--save_path`parameter specifies the path to save the generated text. If not set, it will only be printed.
- `--start`parameter can be used to provide prompts for guiding the model's generation. You can provide multiple prompts in a `prompts.txt` file, with one prompt per line.
