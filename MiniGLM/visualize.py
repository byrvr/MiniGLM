import matplotlib.pyplot as plt


def visualize_loss(train_loss_list, train_interval, val_loss_list, val_interval, dataset, out_dir):
    """
    Visualizes the training and validation loss.

    Parameters:
    - train_loss_list: List of training losses.
    - train_interval: Interval for training loss data points.
    - val_loss_list: List of validation losses.
    - val_interval: Interval for validation loss data points.
    - dataset: Name of the dataset.
    - out_dir: Directory to save the plot.

    Returns:
    None. Saves the plot to the given directory.
    """
    # Calculate x values for training and validation based on intervals
    train_x = [i * train_interval for i in range(len(train_loss_list))]
    val_x = [i * val_interval for i in range(len(val_loss_list))]

    # Plotting the data
    plt.figure(figsize=(10, 6))
    plt.plot(train_x, train_loss_list, label='Training Loss', color='blue')
    plt.plot(val_x, val_loss_list, label='Validation Loss', color='red')

    plt.title(f"Training and Validation Loss for {dataset}")
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.legend()

    # Save the figure
    plt.savefig(f"{out_dir}/loss.png")
    plt.close()


# Dummy data for testing
# train_loss = [0.9, 0.7, 0.5, 0.4, 0.3]
# val_loss = [0.85, 0.65, 0.48]
# visualize_loss(train_loss, 1, val_loss, 2, "combined", ".")
